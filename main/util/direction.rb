module Direction

  NORTH = 'North'
  EAST = 'East'
  WEST = 'West'
  SOUTH = 'South'

  def turn_90(now_direction, angle)
    # 右回転
    if angle.positive?
      if now_direction === NORTH
        EAST
      elsif now_direction === EAST
        SOUTH
      elsif now_direction === WEST
        NORTH
      elsif now_direction === SOUTH
        WEST
      end
    # 左回転
    else
      if now_direction === NORTH
        WEST
      elsif now_direction === EAST
        NORTH
      elsif now_direction === WEST
        SOUTH
      elsif now_direction === SOUTH
        EAST
      end
    end
  end

  def turn_180(now_direction)
    if now_direction === NORTH
      SOUTH
    elsif now_direction === EAST
      WEST
    elsif now_direction === WEST
      EAST
    elsif now_direction === SOUTH
      NORTH
    end
  end

  def turn_270(now_direction, angle)
    turn_90(now_direction, -angle)
  end

end