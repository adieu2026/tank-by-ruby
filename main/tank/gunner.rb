require './human.rb'

class Gunner < Human
  def initialize(name, weight)
    super(name, weight)
  end
end