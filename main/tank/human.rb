class Human

  def initialize(name, weight)
    @name = name
    @weight = weight
  end

  attr_accessor :name, :weight

end