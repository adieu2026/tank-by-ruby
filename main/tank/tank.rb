require '../util/direction.rb'
require './fuel_tank.rb'
require './turret.rb'
# require './gunner.rb'
# require './steerer.rb'

class Tank
  include Direction

  def initialize
    @is_alive = true #破壊されていないかどうか
    @is_running = false #エンジン駆動中かどうか
    @weight = 10000 #kg (1t)
    @total_weight = 0 #kg (本体、燃料、砲弾、人員)
    @x = 0 #m
    @y = 0 #m
    @rest_of_hit_count = 10
    @gunner = nil
    @steerer = nil
    @direction = Direction::NORTH
    @fuel_tank = FuelTank.new
    @turret = Turret.new(@direction)
  end

  attr_accessor :is_alive, :is_running, :direction, :x, :y, :weight,
                :gunner, :steerer, :fuel_tank, :turret, :rest_of_hit_count

  # エラーメッセージ
  INVALID_INPUT = 'INVALID INPUT'

  # 搭乗 
  # 操舵手、砲手1人ずつ
  def get_on(crews)
    return 'No one in the tank!' if crews[:gunner].nil? || crews[:steerer].nil?
    return 'he is not a gunner' unless crews[:gunner].kind_of?(Gunner)
    return 'he is not a steerer' unless crews[:steerer].kind_of?(Steerer)
    @gunner = crews[:gunner]
    @steerer = crews[:steerer]
    "Gunner: #{@gunner.name}, Steerer: #{@steerer.name}"
  end

  # 戦車の状態を確認
  def is_alive?
    return @is_alive if @is_alive
    'The tank is destroyed'
  end

  # 破壊！
  # エンジンがかけられないため、行動不能となる
  def destroy
    @is_alive = false
  end

  # エンジン点火
  def start
    # バリデーション
    return is_alive? unless @is_alive
    return 'The engine is already running' if is_running
    
    # 燃料チェック通れば消費される、だめならreturn
    consume_result = @fuel_tank.consume(10)
    return consume_result if consume_result.kind_of?(String)
    
    @is_running = true
  end

  #  エンジン停止
  def stop
    @is_running = false
  end

  # エンジンの状態を確認
  def is_running?
    return @is_running if @is_running
    'The engine is not running'
  end

  # 前進・後退
  # 正の数は前進、負の数は後退
  # param  distance        [Integer]  進む距離(m)
  # return result          [String]   エラー内容
  # return @total_distance [Integer]  トータルで進んだ距離
  def go(distance)
    # バリデーション
    return is_running? unless @is_running
    return INVALID_INPUT if distance.nil? || !distance.kind_of?(Integer) || distance == 0
    return 'Please input a number that be divisible by 100' if (distance).abs < 100 || distance % 100 != 0

    # 燃料チェック通れば消費される、だめならreturn
    consume_result = @fuel_tank.consume((distance / 100).abs)
    return consume_result if consume_result.kind_of?(String)

    # 現在位置を座標的に示す
    if @direction == Direction::NORTH
      @y += distance
    elsif @direction == Direction::SOUTH
      @y -= distance
    elsif @direction == Direction::EAST
      @x += distance
    elsif @direction == Direction::WEST
      @x -= distance
    end
    "x = #{@x}m, y = #{@y}m"
  end

  # 方向転換（超信地旋回）
  # param angle [Integer]  90 => '右に90度回転', -90 => '左に90度回転'
  def turn(angle)
    # バリデーション
    return is_running? unless @is_running
    return INVALID_INPUT if angle.nil? || !angle.kind_of?(Integer) || ![90, 180, 270, 360].include?((angle).abs)

    # 燃料チェック通れば消費される、だめならreturn
    consume_result = @fuel_tank.consume((angle).abs / 90)
    return consume_result if consume_result.kind_of?(String)

    case angle
    when 360, -360
      @turret.direction = @turret.direction
      @direction
    when 90, -90
      @direction = turn_90(@direction, angle)
      @turret.direction = @turret.turn_90(@turret.direction, angle)
    when 180, -180
      @direction = turn_180(@direction)
      @turret.direction = @turret.turn_180(@turret.direction)
    when 270, -270
      @direction = turn_270(@direction, angle)
      @turret.direction = @turret.turn_270(@turret.direction, angle)
    end
    @direction
  end

  # 被弾
  def attacked
    @is_running = false
    if @rest_of_hit_count > 1
      @rest_of_hit_count -= 1
    else #カウントが0になると同時にfalseにしたい
      @rest_of_hit_count -= 1
      @is_alive = false
    end
  end

  # 砲塔に回転指示を出す
  def turn_turret(angle)
    # バリデーション
    return is_running? unless @is_running
    return INVALID_INPUT if angle.nil? || !angle.kind_of?(Integer) || ![90, 180, 270, 360].include?((angle).abs)
    consume_result = fuel_tank.consume((angle).abs / 90)
    return consume_result if consume_result.kind_of?(String)

    @turret.turn(angle)
  end

  # 砲弾発射
  def shoot
    # バリデーション
    return is_running? unless @is_running
    # 発射する砲弾によって使用燃料が違うので、ここだけ燃料チェックはturret側で行う
    @turret.shoot(@fuel_tank, @x, @y)
  end

  # 総重量を出す
  def get_total_weight
    @total_weight = @weight + @fuel_tank.fuel_amount +
                    @gunner.weight + @steerer.weight + @turret.get_total_weight
  end

end