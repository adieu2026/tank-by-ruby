class FuelTank

  # 容量は最大500Lまで
  # 1L = 1kg
  CAPACITY = 500

  def initialize(fuel_amount = CAPACITY)
    @fuel_amount = fuel_amount unless fuel_amount > CAPACITY
  end

  attr_accessor :fuel_amount


  # 燃料消費
  def consume(amount)
    if amount.nil? || amount <= 0
      'INVALID INPUT'
    elsif @fuel_amount < amount
      'SHORTAGE!'
    else
      @fuel_amount -= amount
    end
  end

  # 燃料補給
  def charge(amount)
    if amount.nil? || amount <= 0
      'INVALID INPUT'
    elsif (@fuel_amount + amount) > CAPACITY
      'OVERFLOWING!'
    else
      @fuel_amount += amount
    end
  end




end