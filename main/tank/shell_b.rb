require "./shell_base.rb"

class ShellB < ShellBase
  def initialize
    shell_info = {:weight => 30, :fuel_necessary => 1, :flying_distance => 2000}
    super(shell_info)
  end
end