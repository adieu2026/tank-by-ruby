require './gunner.rb'
require './steerer.rb'


module Character

  # steerer
  ALEX = Steerer.new('Alex', 50)
  BOB = Steerer.new('Bob', 60)
  CHRIS = Steerer.new('Chris', 70)

  # gunner
  DAVID = Gunner.new('David', 80)
  ERIC = Gunner.new('Eric', 50)

end