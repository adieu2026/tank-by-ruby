require '../util/direction.rb'

class ShellBase
  include Direction

  def initialize(shell_info)
    @weight = shell_info[:weight] #kg
    @fuel_necessary = shell_info[:fuel_necessary] #L
    @flying_distance = shell_info[:flying_distance] #m
  end

  attr_accessor :weight, :fuel_necessary, :flying_distance

  # 砲弾の着地点を出す
  def get_point_of_fire(direction, tank_x, tank_y)
    shell_x = 0
    shell_y = 0
    if direction === Direction::NORTH
      shell_y = tank_y + @flying_distance
      shell_x = tank_x
    elsif direction === Direction::EAST
      shell_y = tank_y
      shell_x = tank_x + @flying_distance
    elsif direction === Direction::WEST
      shell_y = tank_y
      shell_x = tank_x - @flying_distance
    elsif direction === Direction::SOUTH
      shell_y = tank_y - @flying_distance
      shell_x = tank_x
    end
    "point of fire: x = #{shell_x}m, y = #{shell_y}m"
  end

end