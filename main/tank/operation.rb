require "./tank.rb"
require "./shell_a.rb"
require "./shell_b.rb"
require "./character.rb"


tank = Tank.new

# 搭乗
crews = {:gunner => Character::DAVID, :steerer => Character::CHRIS}
tank.get_on(crews)

# 装填、砲弾はAAABBABAABの順
tank.turret.add_shell(ShellA.new)
tank.turret.add_shell(ShellA.new)
tank.turret.add_shell(ShellA.new)
tank.turret.add_shell(ShellB.new)
tank.turret.add_shell(ShellB.new)
tank.turret.add_shell(ShellA.new)
tank.turret.add_shell(ShellB.new)
tank.turret.add_shell(ShellA.new)
tank.turret.add_shell(ShellA.new)
tank.turret.add_shell(ShellB.new)

# operations
# 00 エンジン点火
tank.start
# 01 300m 前進
tank.go(300)
# 02 右90度方向転換
tank.turn(90)
# 03 1500m 前進
tank.go(1500)
# 04 左90度方向転換
tank.turn(-90)
# 05 300m前進
tank.go(300)
# 06 右90度方向転換
tank.turn(90)
# 07 100m前進
tank.go(100)
# 08 左90度方向転換
tank.turn(-90)
# 09 400m後退
tank.go(-400)
# 10 被弾
tank.attacked
# 11 左90度方向転換
tank.start
tank.turn(-90)
# 12 800m前進、燃料補給所に到達
puts "【Q1. 燃料補給所はどこにある？ => #{tank.go(800)}】"
# 13 補給所で20リットル燃料補給
tank.fuel_tank.charge(20)
# 14 左90度方向転換
tank.turn(-90)
# 15 400m前進
tank.go(400)
# 16 180度方向転換
tank.turn(180)
# 17 400m後退
tank.go(400)
# 18 砲塔を右90度回転
tank.turn_turret(90)
# 19 砲弾発射
tank.shoot
# 20 右90度方向転換
tank.turn(90)
# 21 1000m前進、橋に到着
puts "【Q2. 橋はどこにある？ => #{tank.go(1000)}】"
puts "【Q3. 現時点の総重量は？ => #{tank.get_total_weight} kg】"
puts "残燃料：#{tank.fuel_tank.fuel_amount}"
puts "人員： #{tank.gunner.weight + tank.steerer.weight}"
puts "砲弾： #{tank.turret.get_total_weight}"
puts "戦車： #{tank.weight}"
# 22 100m前進、橋を渡り切る
tank.go(100)
# 23 砲塔を右90度回転
tank.turn_turret(90)
# 24 砲弾発射
tank.shoot
# 25 砲弾発射
tank.shoot
# 26 砲塔を右270度回転
tank.turn_turret(270)
# 27 砲弾発射
puts "【Q4. 砲弾はどこまでとんでいく？ => #{tank.shoot}】"
# 28 1000m前進
tank.go(1000)
# 29 被弾
tank.attacked
# 30 右90度方向転換
tank.start
tank.turn(90)
# 31 900m前進
tank.go(900)

puts "【Q5. 戦車の向きは？ => #{tank.direction}】"
puts "【Q6. 砲塔の向きは？ => #{tank.turret.direction}】"
puts "【Q7. 残り何発耐えられる？ => #{tank.rest_of_hit_count}発】"

