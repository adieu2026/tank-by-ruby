require '../util/direction.rb'
require './shell_base.rb'

class Turret
  include Direction

  def initialize(direction)
    @direction = direction
    @shells = []
    @total_weight = 0 #kg 砲弾のトータル重量
  end

  attr_accessor :direction, :shells, :total_weight

  # 装填、10発まで
  def add_shell(new_shell)
    if !new_shell.kind_of?(ShellBase)
      'Only instances of shell are available'
    elsif @shells.count == 10
      'The turret is full'
    else
      @shells.push(new_shell)
    end
  end

  # 方向転換
  # tankとは連動せず砲塔だけ回転する
  def turn(angle)
    case angle
    when 360, -360
      @direction
    when 90, -90
      @direction = turn_90(@direction, angle)
    when 180, -180
      @direction = turn_180(@direction)
    when 270, -270
      @direction = turn_270(@direction, angle)
    end
  end

  # 砲弾発射
  def shoot(fuel_tank, tank_x, tank_y)
    return 'no shell' unless @shells.size > 0
    shell = @shells.shift
    consume_result = fuel_tank.consume(shell.fuel_necessary)
    return consume_result if consume_result.kind_of?(String)

    shell.get_point_of_fire(@direction, tank_x, tank_y)
  end

  # 砲塔の重量（= 砲弾の重量）を取得
  def get_total_weight
    @total_weight = 0 # 毎回初期化しないとひたすら足されていく、、、
    @shells.each {|shell| @total_weight += shell.weight}
    @total_weight
  end

end