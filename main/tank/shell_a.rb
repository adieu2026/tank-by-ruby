require_relative "./shell_base.rb"

class ShellA < ShellBase
  def initialize
    shell_info = {:weight => 50, :fuel_necessary => 2, :flying_distance => 1000}
    super(shell_info)
  end
end