require 'test/unit'
require '../../main/tank/fuel_tank.rb'

class TestFuelTank < Test::Unit::TestCase
  def setup
    @fuel_tank = FuelTank.new
  end

  def test_consume
    assert_equal 'INVALID INPUT', @fuel_tank.consume(nil)
    assert_equal 'INVALID INPUT', @fuel_tank.consume(-1)
    assert_equal 'INVALID INPUT', @fuel_tank.consume(0)

    assert_equal 450, @fuel_tank.consume(50)
    assert_equal 0, @fuel_tank.consume(450)
    assert_equal 'SHORTAGE!', @fuel_tank.consume(1)
  end

  def test_charge
    assert_equal 'INVALID INPUT', @fuel_tank.charge(nil)
    assert_equal 'INVALID INPUT', @fuel_tank.charge(-1)
    assert_equal 'INVALID INPUT', @fuel_tank.charge(0)

    # 初期容量がどうであろうとCAPACITYまでは補給可能である
    @fuel_tank_for_test_charge = FuelTank.new(450)
    assert_equal 500, @fuel_tank_for_test_charge.charge(50)
    assert_equal 'OVERFLOWING!', @fuel_tank_for_test_charge.charge(1)

  end
end