require 'test/unit'
require '../../main/tank/turret.rb'
require '../../main/tank/shell_a.rb'
require '../../main/tank/shell_b.rb'
require '../../main/tank/fuel_tank.rb'

class TestTurret < Test::Unit::TestCase

  def setup
    @turret = Turret.new(Direction::NORTH)
  end

  def test_add_shell
    assert_equal 'Only instances of shell are available', @turret.add_shell('hoge')
    assert_equal 'Only instances of shell are available', @turret.add_shell(nil)

    # いろんな書き方してみた
    shell_a = ShellA.new
    assert_equal 50, shell_a.weight
    assert_equal 1, @turret.add_shell(shell_a).count
    # インデックス番号で直接アクセスできる
    assert_equal 30, @turret.add_shell(ShellB.new)[1].weight

    # 10個以上は装填できない
    8.times {@turret.add_shell(ShellA.new)}
    assert_equal 10, @turret.shells.count
    assert_equal 'The turret is full', @turret.add_shell(ShellB.new)

    # 順番もあってる
    assert_equal ShellB, @turret.shells[1].class
    assert_equal ShellA, @turret.shells[9].class
  end

  def test_turn
    # 北からスタート
    assert_equal 'North', @turret.direction

    assert_equal 'East', @turret.turn(90)
    assert_equal 'West', @turret.turn(-180)
    assert_equal 'South', @turret.turn(270)
    assert_equal 'North', @turret.turn(180)
    assert_equal 'North', @turret.turn(-360)
  end

  def test_shoot
    @turret.shells = [ShellB.new, ShellB.new, ShellB.new, ShellB.new, ShellB.new,
                      ShellA.new, ShellA.new, ShellA.new, ShellA.new, ShellA.new,]
    assert_equal "point of fire: x = 0m, y = 2000m", @turret.shoot(FuelTank.new, 0, 0)
    assert_equal "point of fire: x = 1000m, y = 0m", @turret.shoot(FuelTank.new, 1000, -2000)

    # 砲弾使い切る
    8.times {@turret.shoot(FuelTank.new, 0, 0)}

    assert_equal 'no shell', @turret.shoot(FuelTank.new, 0, 0)
  end

  def test_get_total_weight
    # 最初は空
    assert_equal 0, @turret.get_total_weight

    @turret.shells = [ShellB.new]
    assert_equal 30, @turret.get_total_weight

    @turret.shells = [ShellB.new, ShellB.new, ShellB.new, ShellB.new, ShellB.new,
                      ShellA.new, ShellA.new, ShellA.new, ShellA.new, ShellA.new,]
    assert_equal 10, @turret.shells.count
    assert_equal 400, @turret.get_total_weight
  end
end