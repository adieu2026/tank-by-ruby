require 'test/unit'
require '../../main/tank/shell_base.rb'
require '../../main/util/direction.rb'

class TestShellBase < Test::Unit::TestCase
  include Direction

  def setup
    @shell = ShellBase.new({:weight => 100, :fuel_necessary => 10, :flying_distance => 1000})
  end

  def test_get_point_of_fire
    assert_equal "point of fire: x = 0m, y = 1000m",
      @shell.get_point_of_fire(Direction::NORTH, 0, 0)

    assert_equal "point of fire: x = 100m, y = -1000m",
      @shell.get_point_of_fire(Direction::SOUTH, 100, 0)

    assert_equal "point of fire: x = 1100m, y = 500m",
      @shell.get_point_of_fire(Direction::EAST, 100, 500)

    assert_equal "point of fire: x = -1100m, y = -1000m",
      @shell.get_point_of_fire(Direction::WEST, -100, -1000)
  end
end
