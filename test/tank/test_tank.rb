require 'test/unit'
require '../../main/tank/tank.rb'
require '../../main/util/direction.rb'
require '../../main/tank/gunner.rb'
require '../../main/tank/steerer.rb'

class TestTank < Test::Unit::TestCase

  def setup
    @tank = Tank.new
  end

  def test_get_on
    gunner = Gunner.new('hoge', 100)
    steerer = Steerer.new('piyo', 50)
    crews = {:steerer => steerer, :gunner => gunner}

    assert_equal "Gunner: hoge, Steerer: piyo", @tank.get_on(crews)

    # 空が含まれるとき
    crews = {}
    assert_equal 'No one in the tank!', @tank.get_on(crews)
    crews = {:steerer => steerer}
    assert_equal 'No one in the tank!', @tank.get_on(crews)
    crews = {:gunner => gunner}
    assert_equal 'No one in the tank!', @tank.get_on(crews)

    # 砲手、操舵手がちぐはぐなとき
    crews = {:gunner => steerer, :steerer => gunner}
    assert_equal 'he is not a gunner', @tank.get_on(crews)
    crews = {:gunner => gunner, :steerer => gunner}
    assert_equal 'he is not a steerer', @tank.get_on(crews)
  end

  def test_is_alive?
    @tank.is_alive = false
    assert_equal 'The tank is destroyed', @tank.is_alive?
    @tank.is_alive = true
    assert_equal true, @tank.is_alive?
  end

  def test_destroy
    # 何度呼んでもfalse
    @tank.is_alive = true
    assert_equal false, @tank.destroy
    @tank.is_alive = false
    assert_equal false, @tank.destroy
  end

  def test_start
    assert_equal true, @tank.start
    assert_equal 490, @tank.fuel_tank.fuel_amount

    assert_equal 'The engine is already running', @tank.start

    @tank.is_alive = false
    assert_equal 'The tank is destroyed', @tank.start
  end

  def test_stop
    # 何回呼んでもfalse
    assert_equal false, @tank.stop
    assert_equal false, @tank.stop
  end

  def test_is_running?
    @tank.is_running = false
    assert_equal 'The engine is not running', @tank.is_running?
    @tank.is_running = true
    assert_equal true, @tank.is_running?
  end

  def test_go
    # 初期状態はエンジン止まってる
    assert_equal 'The engine is not running', @tank.go(100)
    @tank.is_running = true

    # nil, 0, 整数以外はアウト
    assert_equal 'INVALID INPUT', @tank.go(nil)
    assert_equal 'INVALID INPUT', @tank.go(0)
    assert_equal 'INVALID INPUT', @tank.go('hoge')

    # 100で割り切れる整数以外は許容しない
    assert_equal 'Please input a number that be divisible by 100', @tank.go(10)
    assert_equal 'Please input a number that be divisible by 100', @tank.go(101)

    assert_equal 'x = 0m, y = 500m', @tank.go(500)
    assert_equal 'x = 0m, y = 0m', @tank.go(-500)
    assert_equal 'x = 0m, y = 49000m', @tank.go(49000)

    # この段階で燃料は0のはず
    assert_equal 'SHORTAGE!', @tank.go(100)
    assert_equal 49000, @tank.y
  end
  
  def test_turn
    # 初期状態はエンジン止まってる
    assert_equal 'The engine is not running', @tank.turn(90)
    @tank.is_running = true

    # バリデーション
    assert_equal 'INVALID INPUT', @tank.turn(nil)
    assert_equal 'INVALID INPUT', @tank.turn(0)
    assert_equal 'INVALID INPUT', @tank.turn(5)
    assert_equal 'INVALID INPUT', @tank.turn('hoge')

    # 戦車本体と一緒に砲塔の向く先も変化する
    # 砲塔自体は動かないので燃料は消費しない
    assert_equal 'North', @tank.direction
    assert_equal 'North', @tank.turret.direction
    assert_equal 'East', @tank.turn(90)
    assert_equal 'East', @tank.turret.direction
    assert_equal 499, @tank.fuel_tank.fuel_amount
    assert_equal 'West', @tank.turn(-180)
    assert_equal 'West', @tank.turret.direction
    assert_equal 497, @tank.fuel_tank.fuel_amount

    @tank.turret.direction = Direction::NORTH
    assert_equal 'South', @tank.turn(270)
    assert_equal 'West', @tank.turret.direction
    assert_equal 'North', @tank.turn(180)
    assert_equal 'East', @tank.turret.direction
    assert_equal 'North', @tank.turn(-360)
    assert_equal 'East', @tank.turret.direction

    # 燃料使い切る
    122.times { @tank.turn(360) }
    assert_equal 'SHORTAGE!', @tank.turn(90)
    assert_equal 'North', @tank.direction
    assert_equal 'East', @tank.turret.direction
  end

  def test_attacked
    assert_equal 9, @tank.attacked

    9.times { @tank.attacked }
    assert_equal 0, @tank.rest_of_hit_count
    assert_equal false, @tank.is_alive
  end

  def test_turn_turret
    # 初期状態はエンジン止まってる
    assert_equal 'The engine is not running', @tank.turn_turret(90)
    @tank.is_running = true

    # バリデーション
    assert_equal 'INVALID INPUT', @tank.turn_turret(nil)
    assert_equal 'INVALID INPUT', @tank.turn_turret(0)
    assert_equal 'INVALID INPUT', @tank.turn_turret(5)
    assert_equal 'INVALID INPUT', @tank.turn_turret('hoge')

    # turretの向きが変わっても本体の向きは変わらない
    assert_equal 'North', @tank.direction
    assert_equal 'North', @tank.turret.direction
    assert_equal 'East', @tank.turn_turret(90)
    assert_equal 'North', @tank.direction
    assert_equal 499, @tank.fuel_tank.fuel_amount
    assert_equal 'West', @tank.turn_turret(-180)
    assert_equal 'North', @tank.direction
    assert_equal 497, @tank.fuel_tank.fuel_amount
    assert_equal 'South', @tank.turn_turret(270)
    assert_equal 'North', @tank.direction
    assert_equal 'North', @tank.turn_turret(180)
    assert_equal 'North', @tank.direction
    assert_equal 'North', @tank.turn_turret(-360)
    assert_equal 'North', @tank.direction

    # 燃料使い切る
    122.times { @tank.turn_turret(360) }
    assert_equal 'SHORTAGE!', @tank.turn_turret(90)
    assert_equal 'North', @tank.turret.direction
  end

  def test_get_total_weight
    @tank.weight = 100
    @tank.fuel_tank.fuel_amount = 100
    @tank.turret.total_weight = 100
    @tank.gunner = Gunner.new('test', 100)
    @tank.steerer = Steerer.new('test', 100)

    assert_equal 500, @tank.get_total_weight
  end

end